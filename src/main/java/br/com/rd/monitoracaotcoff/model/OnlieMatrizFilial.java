package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class OnlieMatrizFilial implements Serializable {

    private Integer cdFilial;
    private String nmFantasia;
    private Integer cdLogomarca;

}
