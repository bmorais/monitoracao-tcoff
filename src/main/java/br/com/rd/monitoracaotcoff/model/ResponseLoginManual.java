package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class ResponseLoginManual implements Serializable {

    private String octetoIp;
    private int cdFilial;
}
