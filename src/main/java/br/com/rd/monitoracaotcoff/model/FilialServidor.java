package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FilialServidor {

    private int cdFilial;

    private String dsIp;

    private String dsVersao;
}
