package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Monitoracao {

    private int nrCaixa;

    private int cdFilial;

    private int cdParametroPdv;

    private String vlParametroPdv;

}
