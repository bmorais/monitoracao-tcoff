package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class TcOffErro {

    private int cdFilial;

    private String host;

    private String dsErro;

    private String dataHoraAtualizacao;

    public TcOffErro() {
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        setDataHoraAtualizacao(sf.format(new Date()));
    }
}
