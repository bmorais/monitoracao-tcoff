package br.com.rd.monitoracaotcoff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class MonitoracaoFilialCaixaEntity {

    private Integer				cdFilial;

    private Integer				nrCaixa;

    private Map<Integer, String> parametros = new HashMap<>();

    private MonitoracaoFilialCaixaEntity monitoracaoFilialCaixaEntity;

    public MonitoracaoFilialCaixaEntity() {
        setNrCaixa(0);
    }

    public Integer getCdFilial() {
        return cdFilial;
    }

    public void setCdFilial(Integer cdFilial) {
        this.cdFilial = cdFilial;
    }

    public Integer getNrCaixa() {
        return nrCaixa;
    }

    public void setNrCaixa(Integer nrCaixa) {
        this.nrCaixa = nrCaixa;
    }

    public Map<Integer, String> getParametros() {
        return parametros;
    }

    public void setParametros(Map<Integer, String> parametros) {
        this.parametros = parametros;
    }

    public MonitoracaoFilialCaixaEntity getMonitoracaoFilialCaixaEntity() {
        return monitoracaoFilialCaixaEntity;
    }

    public void setMonitoracaoFilialCaixaEntity(MonitoracaoFilialCaixaEntity monitoracaoFilialCaixaEntity) {
        this.monitoracaoFilialCaixaEntity = monitoracaoFilialCaixaEntity;
    }

    @Override
    public String toString() {
        return "MonitoracaoFilialCaixaEntity{" +
                "cdFilial=" + cdFilial +
                ", nrCaixa=" + nrCaixa +
                ", parametros=" + parametros +
                ", monitoracaoFilialCaixaEntity=" + monitoracaoFilialCaixaEntity +
                '}';
    }
}