package br.com.rd.monitoracaotcoff.infrastructure;

import retrofit2.Call;
import retrofit2.http.GET;
public interface TcOffClient {

    @GET("/tcOffWeb/rest/tcoff")
    Call<Object> checkAlive();

    @GET("/tcOffWeb/rest/produto/id?cdFilial=1007&cliente=%7B\"nrCpf\":\"48806733826\",%20\"%7D%7D&faixaPreco=60&isNumber=true&produto=79313")
    Call<Object> checkProduct();

    @GET("/tcOffWeb/Alive")
    Call<String> checkAliveAntigo();

}
