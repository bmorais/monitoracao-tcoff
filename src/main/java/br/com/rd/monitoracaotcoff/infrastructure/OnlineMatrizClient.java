package br.com.rd.monitoracaotcoff.infrastructure;

import br.com.rd.monitoracaotcoff.model.MonitoracaoFilialCaixaEntity;
import br.com.rd.monitoracaotcoff.model.OnlieMatrizFilial;
import br.com.rd.monitoracaotcoff.model.ResponseLoginManual;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

public interface OnlineMatrizClient {

    @POST("/online-matriz-service/monitoracao")
    Call<Void> postMonitoracaoController(@Body MonitoracaoFilialCaixaEntity monitoracaoFilialCaixaEntity);

    @GET("/online-matriz-service/filiais")
    Call<List<OnlieMatrizFilial>> getFilialController();

    @POST("/online-matriz-service/login-manual")
    Call<ResponseLoginManual> postLoginManual(@Query("cdFilial") int cdFilial);


}
