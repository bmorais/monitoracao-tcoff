package br.com.rd.monitoracaotcoff.application.config;

import br.com.rd.monitoracaotcoff.application.util.HttpUtil;
import br.com.rd.monitoracaotcoff.business.TcOffBusiness;
import br.com.rd.monitoracaotcoff.infrastructure.OnlineMatrizClient;
import br.com.rd.monitoracaotcoff.infrastructure.TcOffClient;
import br.com.rd.monitoracaotcoff.model.MonitoracaoFilialCaixaEntity;
import br.com.rd.monitoracaotcoff.model.OnlieMatrizFilial;
import br.com.rd.monitoracaotcoff.model.ResponseLoginManual;
import br.com.rd.monitoracaotcoff.model.TcOffErro;
import org.jetbrains.annotations.NotNull;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class BatchConfig {

    @Bean
    public Job job(JobRepository repository, Step step){
        return new JobBuilder("job", repository )
                .start(step)
                .build();
    }

    @Bean
    public Step step (JobRepository jobRepository , PlatformTransactionManager transactionManager){
        return new StepBuilder("step", jobRepository)
                .tasklet((StepContribution s, ChunkContext c) -> {
                    System.out.println("INICIANDO APLICAÇÃO..");
                    try {

                        String onlineMatrizService = "http://10.1.50.68:8081";
                        Retrofit retrofitOnlineMatrizService = HttpUtil.getClient(onlineMatrizService);
                        OnlineMatrizClient serviceOnlineMatriz = retrofitOnlineMatrizService.create(OnlineMatrizClient.class);

                        List<OnlieMatrizFilial> responseFiliais = serviceOnlineMatriz.getFilialController().execute().body();
                        List<ResponseLoginManual> listFiliais = new ArrayList<>();

                        for(OnlieMatrizFilial o : responseFiliais){
                            ResponseLoginManual responseLoginManual = serviceOnlineMatriz.postLoginManual(o.getCdFilial()).execute().body();
                            if(responseLoginManual != null){
                                listFiliais.add(responseLoginManual);
                            }
                        }

                        if(!listFiliais.isEmpty()){
                            System.out.println(String.format("QTD FILIAL [%s]", listFiliais.size()));
                            listFiliais.forEach(f -> {
                                if(f != null) {

                                    TcOffErro tcOffErro = new TcOffErro();
                                    tcOffErro.setCdFilial(f.getCdFilial());
                                    tcOffErro.setHost(formatIp(f));

                                    String url = "http://%s:8080";
                                    url = String.format(url, formatIp(f));

                                    Retrofit retrofitTcOff = HttpUtil.getAliveTcOff(url);
                                    TcOffClient serviceTcOff = retrofitTcOff.create(TcOffClient.class);
                                    try {

                                        Response<String> response = serviceTcOff.checkAliveAntigo().execute();
                                        tcOffErro.setDsErro(new TcOffBusiness().converterErro(response.code(),response.body()));

                                        Map<Integer, String> parametroPdv = new HashMap<>();
                                        MonitoracaoFilialCaixaEntity monitoracaoFilialCaixaEntity = new MonitoracaoFilialCaixaEntity();
                                        String erro = HttpUtil.convertToJson(tcOffErro);
                                        parametroPdv.put(127,"{\"cdFilial\":1047,\"host\":\"10.240.58.12\",\"dsErro\":\"200 - Conteudo invalido: OK|V3.3\",\"dataHoraAtualizacao\":\"28/02/2024 15:06\"}");
//                                        parametroPdv.put(127,erro);

                                        monitoracaoFilialCaixaEntity.setCdFilial(f.getCdFilial());
                                        monitoracaoFilialCaixaEntity.setParametros(parametroPdv);

//
                    //                    MonitoracaoFilialCaixaEntity monitoracaoFilialCaixaEntity = new MonitoracaoFilialCaixaEntity();
                    //                    monitoracaoFilialCaixaEntity.setCdFilial(1047);
                    //                    monitoracaoFilialCaixaEntity.setParametros(parametroPdv);

                                        serviceOnlineMatriz.postMonitoracaoController(monitoracaoFilialCaixaEntity).execute();

//                                        if (response.isSuccessful()) {
//                                            System.out.println(String.format("Filial[%s] vTcOff[%s]",f.getCdFilial(),response.body()));
//                                        }
                                    } catch (Exception e ){

                                    }
                                }

                            });
                        }



//            serviceOnlineMatriz.postMonitoracaoController(monitoracaoFilialCaixaEntity).execute();


                    }catch (Exception e) {

                    }
                    System.out.println("FINALIZANDO APLICACAO..");
                    return RepeatStatus.FINISHED;
                }, transactionManager).build();
    }

    @NotNull
    private static String formatIp(ResponseLoginManual f) {
        return f.getOctetoIp() + ".12";
    }




}
