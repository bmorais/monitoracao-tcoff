package br.com.rd.monitoracaotcoff.application.util;

import br.com.rd.monitoracaotcoff.model.TcOffErro;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public class HttpUtil {

    public static Retrofit getClient(String url) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(2, TimeUnit.SECONDS);
            httpClient.readTimeout(2, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }


    public static Retrofit getAliveTcOff(String url) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(2, TimeUnit.SECONDS);
        httpClient.readTimeout(2, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(new StringConverterFactory())
                .client(httpClient.build())
                .build();
    }

    public static <T> String convertToJson(T object) {
        Gson gson = new Gson();
        return StringUtils.abbreviate(gson.toJson(object), 799);
    }

}
