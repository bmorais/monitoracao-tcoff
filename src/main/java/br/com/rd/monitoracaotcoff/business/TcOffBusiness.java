package br.com.rd.monitoracaotcoff.business;

public class TcOffBusiness {

    private static final String ERRO_SEM_TELA_DE_ALIVE = "HTTP STATUS 404";

    private static final String ERRO_TIME_OUT = "TIMED OUT";

    private static final String ERRO_SEM_CONEXAO = "CONNECTION REFUSED";

    private static final String ERRO_VALIDAR_FILIAL = "ERRO - AO CONSULTAR FILIAL";

    private static final String ERRO_CONSULTAR_PRODUTO = "ERRO - AO CONSULTAR PRODUTO";

    public static String converterErro(int codigoErro, String messageErro) {
        StringBuilder erro = new StringBuilder();
        if (messageErro != null) {
            messageErro = messageErro.toUpperCase();
            if (messageErro.contains(ERRO_SEM_TELA_DE_ALIVE)) {
                erro.append(codigoErro);
                erro.append(" - Sem monitoracao");
            } else if (messageErro.contains(ERRO_SEM_CONEXAO)) {
                erro.append("Sem conexao");
            } else if (messageErro.contains(ERRO_TIME_OUT)) {
                erro.append("Tempo esgotado");
            } else if (messageErro.contains(ERRO_VALIDAR_FILIAL)) {
                erro.append("ERRO - AO CONSULTAR FILIAL");
            } else if (messageErro.contains(ERRO_CONSULTAR_PRODUTO)) {
                erro.append(ERRO_CONSULTAR_PRODUTO);
            } else {
                erro.append(codigoErro);
                erro.append(" - Conteudo invalido: ");
                erro.append(messageErro);
            }
        }
        return erro.toString();
    }

}
