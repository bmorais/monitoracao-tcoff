package br.com.rd.monitoracaotcoff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoracaoTcoffApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoracaoTcoffApplication.class, args);
	}

}
